function inicializar(){

    let vo= document.getElementById('Vo');
    let wo= document.getElementById('Wo');
 
    let canvas = document.getElementById('canvas');
    let ctx = canvas.getContext('2d');
   

    ctx.save();
    ctx.beginPath();
    ctx.globalAlpha = 0.0;
    ctx.fillRect(10,500,60,-30);
    ctx.translate(10, 500);
    ctx.rotate((Math.PI / 180) *(360-wo.value));
    ctx.translate(-20,-500);
    ctx.globalAlpha = 1.0;
    ctx.fillStyle = 'orange';
    ctx.fillRect(20,500,60,-30);
    ctx.fillStyle = 'black';
    ctx.fillRect(23,496,52,-22);
    ctx.restore();

   
    let x= 10;
    let y= 480;
    let xo= x;        
    let yo= y;
    let gravedad=9.8;
    let tiempo= 0;
    let t= (2*vo.value*(Math.sin((Math.PI/180)*(wo.value))))/gravedad;
    let radius= 10;

        do{
            x= xo + (vo.value*(Math.cos((Math.PI/180)*(360-wo.value)))*tiempo);
            y= yo + (vo.value*(Math.sin((Math.PI/180)*(360-wo.value)))*tiempo)+((gravedad*(tiempo*tiempo))/2);
            ctx.beginPath();
            ctx.arc(x, y, radius, 0, Math.PI * 2, true);
            ctx.closePath();
            ctx.fillStyle = 'yellow';
            ctx.fill();
            tiempo=tiempo+0.5;
        }while(tiempo<=t)
    }

